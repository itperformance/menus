<?php

namespace Caffeinated\Menus;

use Caffeinated\Menus\Builder;
use Illuminate\Support\Facades\Request;

class SidebarMenuBuilder extends Builder
{
    /**
     * Renders the menu as an unordered list.
     *
     * @param  array  $attributes
     * @return string
     */
    public function asUl($attributes = array())
    {
        return "<ul{$this->attributes($attributes)}>{$this->render('ul')}</ul>";
    }

    /**
     * Generate the menu items as list items, recursively.
     *
     * @param  string  $type
     * @param  int     $parent
     * @return string
     */
    protected function render($type = 'ul', $parent = null)
    {
        $items   = '';
        $itemTag = in_array($type, ['ul', 'ol']) ? 'li' : $type;

        foreach ($this->whereParent($parent) as $item) {
            $active = false;

            if ($item->hasChildren()) $active = $this->isActive($item, $active);

            if ($active) $attr = 'class="active open"';
            else $attr = $item->attributes();

            $items .= "<{$itemTag} $attr>";

            if ($item->link) {
                $items .= "<a{$this->attributes($item->link->attr())} href=\"{$item->url()}\">{$item->title}</a>";
            } else {
                $items .= $item->title;
            }

            if ($item->hasChildren()) {
                if ($active) $items .= "<{$type} class='submenu nav-open nav-show' style='display:block'>";
                else $items .= "<{$type} class='submenu nav-open nav-hide'>";
                $items .= $this->render($type, $item->id);
                $items .= "</{$type}>";
            }

            $items .= "</{$itemTag}>";

            if ($item->divider) {
                $items .= "<{$itemTag}{$this->attributes($item->divider)}></{$itemTag}>";
            }
        }

        return $items;
    }

    public function isActive($item, $active)
    {
        $childrens = $item->children();

        foreach ($childrens as $child)
        {
            if ($child->hasChildren())
                return $this->isActive($child, $active);

            if ($child->link->path['url'] == Request::path())
                return true;
        }

        return false;
    }
}